
 JAMF is a remote computer management system that is designed 
 to regulate computer settings and preferences to every company computer.

 At Olive we use JAMF to automatically set lockout timers, computer password requirements,
Sophos anti-virus settings and automatic computer updates.




This repository is shared with everyone at Olive to ensure better quality control over package and script deployments.

V.1.0






Contact @Daniel Amar or @Justin Jayjohn on Slack for more questions
#!/bin/bash
#Author: Daniel Amar
#Description: This script is used to automatically change computer names based off of serial numbers. requires the use of temporary
#             files that store a user's name and serial number. This program takes a computer's serial number, finds the name and changes it accordingly.



users="/Users/"
slash="/"
tempFolder="temp"
userName=$3$slash

pathToPackage=$users$userName #pathToPackage = /Users/$3/ ($3 being the username parameter)

#Finds computer model serial number
MODEL=$( system_profiler SPHardwareDataType | grep 'Model Name' | awk -F ': ' '{print $2}' )
SERIAL=$( system_profiler SPHardwareDataType | grep Serial |  awk '{print $NF}' )

#Name and SNs all listed out 
namesFile="temp/FLNames.txt" 
snsFile="temp/SNs.txt"		 

names=$pathToPackage$namesFile #path is /Users/$3/temp/FLNames.txt
sns=$pathToPackage$snsFile	   #path is /Users/$3/temp/SNs.txt

#Declares an integer
declare -i count #declare count integer
count=1

declare -i realLine #declare reference line integer
realLine=-1

declare -i negOne   #declare integer to compare
negOne=-1

#Finds location of Name thru the serial number
#If the line corresponds with a computer serial number, the line location is saved
while IFS= read -r line 
do
   if [ "$line" == "$SERIAL" ]
   then
	 realLine=$count
	 break
   else
	count=count+1
   fi
done < $sns

#if realLine didn't change it's value, we know that the serial number couldn't be found
if [ "$realLine" == "$negOne" ]
then
	nameSaved="No SN"
    echo "Your name couldn't be found and you have a $MODEL"
    sudo jamf setComputerName -name "$nameSaved's $MODEL"
else
	#Declares a second counter
	declare -i countb
	countb=1

    #While there are lines to read in the names file, read a line and compare the line number
    to the one determined in the last loop. If a match is found name is saved.
	while IFS= read -r nameLine
	do
   		if (( "$countb" == "$realLine" ))
   		then
			nameSaved=$nameLine 
			break 
   		else
			countb=countb+1
  		fi
	done < $names

    #output name and serial number, then change computer name
	echo "$nameSaved is your name and you have a $MODEL"
	sudo jamf setComputerName -name "$nameSaved's $MODEL"
fi

#Removes temporary files stored by package 
#[an error with this line previously took out 7 peoples' entire home directories, be careful.]
rm $pathToPackage$tempFolder 
exit 0		## Success





